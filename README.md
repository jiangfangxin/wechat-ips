# Project build for
This project allow you to check an ip is belong to wechat official site. 
# Introduction
1. Copy contents of folder `wecaht-ips` to your web root folder.
2. Open link `http://www.example.com/index.php` in browser.
3. Fill the ip field and check if it's belong to wechat.

# Usage
There are two method maybe useful for you in file `application/models/wechat_ips_model.php`:
1. `is_wechat_ip($ip)` - Check if `$ip` is belong to wechat.
2. `get_wechat_ips()`  - Get all wechat official ips. 

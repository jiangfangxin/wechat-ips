<?php

class Wechat_ips_model extends CI_Model
{
    private $_app_id            = 'wx5deb7688ae51c8db';
    private $_app_secret        = '3ee4b57027d33751a2bef60aa59170eb';

    private $_access_token_time = 7200;  // 2 * 60 * 60
    private $_access_token_url  = 'https://api.weixin.qq.com/cgi-bin/token';
    private $_wechat_ips_url    = 'https://api.weixin.qq.com/cgi-bin/getcallbackip';

    private function _get_access_token_file()
    {
        return __DIR__ . "/../cache/access_token_cache.txt";
    }

    private function _get_wechat_ips_file()
    {
        return __DIR__ . '/../cache/wechat_ips_cache.txt';
    }

    public function is_wechat_ip($ip)
    {
        $wechat_ips = unserialize(file_get_contents($this->_get_wechat_ips_file()));
        if (is_array($wechat_ips) && in_array($ip, $wechat_ips)) {
            return true;
        }

        $wechat_ips = $this->get_wechat_ips();
        if ($wechat_ips === false) {
            return -1;
        }

        return in_array($ip, $wechat_ips) ? true : false;
    }

    public function get_wechat_ips()
    {
        $access_token = $this->get_access_token();
        if ($access_token === false) {
            return false;
        }

        $query['access_token'] = $access_token;
        $result                = $this->curl_get($this->_wechat_ips_url, $query);
        $result                = json_decode($result, true);

        if (isset($result['ip_list'])) {
            $wechat_ips_cache = $result['ip_list'];

            $saved = file_put_contents(
                $this->_get_wechat_ips_file(),
                serialize($wechat_ips_cache)
            );

            if (!$saved) {
                error_log('Cannot update the wechat ips to cache file.');
            }
            return $result['ip_list'];
        }

        return false;
    }

    public function get_access_token()
    {
        $access_token_cache = unserialize(file_get_contents($this->_get_access_token_file()));

        if (!is_array($access_token_cache)) {
            $access_token_cache = [
                'access_token' => '',
                'time'         => ''
            ];
        }

        $last_time = $access_token_cache['time'];

        if ($last_time != '' && is_numeric($last_time)) {
            $time = time();
            if ($time - $last_time < $this->_access_token_time) {
                return $access_token_cache['access_token'];
            }
        }

        $query['grant_type'] = 'client_credential';
        $query['appid']      = $this->_app_id;
        $query['secret']     = $this->_app_secret;
        $result              = $this->curl_get($this->_access_token_url, $query);
        $result              = json_decode($result, true);

        if (isset($result['access_token'])) {
            $access_token_cache['time']         = time();
            $access_token_cache['access_token'] = $result['access_token'];

            $saved = file_put_contents(
                $this->_get_access_token_file(),
                serialize($access_token_cache)
            );

            if (!$saved) {
                error_log('Cannot write the new access token to cache file.');
            }
            return $result['access_token'];
        }

        return false;
    }

    public function curl_get($url = false, $query = [])
    {
        if ($url === false || !is_array($query)) {
            return false;
        }

        $sep = '?';
        foreach ($query as $key => $value) {
            $url .= $sep . $key . '=' . $value;
            $sep  = '&';
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        if (curl_errno($ch) != 0) {
            curl_close($ch);
            return false;
        }

        curl_close($ch);
        return $result;
    }
}
